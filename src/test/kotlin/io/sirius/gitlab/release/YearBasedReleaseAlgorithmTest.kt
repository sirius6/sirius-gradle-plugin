package io.sirius.gitlab.release

import io.mockk.every
import io.mockk.mockk
import io.sirius.gitlab.JobContext
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Tag
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Year

class YearBasedReleaseAlgorithmTest {

    private lateinit var algorithm: YearBasedReleaseAlgorithm
    private lateinit var gitLabApi: GitLabApi
    private lateinit var currentYear: Year
    private lateinit var context: JobContext

    @BeforeEach
    fun before() {
        context = JobContext(
            projectId = "test123",
            projectName = "Test",
            gitlabUrl = "http://localhost:8080",
            buildBranch = "develop",
            accessToken = "123456789"
        )
        gitLabApi = mockk(relaxed = true)
        algorithm = YearBasedReleaseAlgorithm()
        currentYear = Year.now()
    }

    @Test
    fun testVeryFirstVersion() {
        every { gitLabApi.tagsApi.getTags(
            context.projectId,
            Constants.TagOrderBy.UPDATED,
            Constants.SortOrder.DESC,
            "^$currentYear."
        ) } returns emptyList()

        assertEquals("$currentYear.1", algorithm.getNextReleaseVersion(context, gitLabApi))
    }

    @Test
    fun testIncrementVersion() {
        every { gitLabApi.tagsApi.getTags(
            context.projectId,
            Constants.TagOrderBy.UPDATED,
            Constants.SortOrder.DESC,
            "^$currentYear."
        ) } returns listOf(
            createTag("$currentYear.2"),
            createTag("$currentYear.1"),
        )

        assertEquals("$currentYear.3", algorithm.getNextReleaseVersion(context, gitLabApi))
    }

    @Test
    fun testWithReleaseCandidate() {
        every { gitLabApi.tagsApi.getTags(
            context.projectId,
            Constants.TagOrderBy.UPDATED,
            Constants.SortOrder.DESC,
            "^$currentYear."
        ) } returns listOf(
            createTag("$currentYear.3.RC1"),
            createTag("$currentYear.2"),
            createTag("$currentYear.1"),
        )

        assertEquals("$currentYear.3", algorithm.getNextReleaseVersion(context, gitLabApi))
    }

    @Test
    fun testWithHotfix() {
        every { gitLabApi.tagsApi.getTags(
            context.projectId,
            Constants.TagOrderBy.UPDATED,
            Constants.SortOrder.DESC,
            "^$currentYear."
        ) } returns listOf(
            createTag("$currentYear.2.2"),
            createTag("$currentYear.2.1"),
            createTag("$currentYear.2"),
            createTag("$currentYear.1"),
        )

        assertEquals("$currentYear.3", algorithm.getNextReleaseVersion(context, gitLabApi))
    }

    private fun createTag(name: String): Tag {
        val tag = Tag()
        tag.name = name
        return tag
    }

}