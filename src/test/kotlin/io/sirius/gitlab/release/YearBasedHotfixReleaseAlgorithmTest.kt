package io.sirius.gitlab.release

import io.mockk.every
import io.mockk.mockk
import io.sirius.gitlab.JobContext
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Tag
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class YearBasedHotfixReleaseAlgorithmTest {

    private lateinit var algorithmYearBased: YearBasedHotfixReleaseAlgorithm
    private lateinit var gitLabApi: GitLabApi
    private lateinit var context: JobContext

    @BeforeEach
    fun before() {
        context = JobContext(
            projectId = "test123",
            projectName = "Test",
            gitlabUrl = "http://localhost:8080",
            buildBranch = "develop",
            accessToken = "123456789"
        )
        gitLabApi = mockk(relaxed = true)
        algorithmYearBased = YearBasedHotfixReleaseAlgorithm()
    }

    @Test
    fun testVeryFirstHotfix() {
        every {
            gitLabApi.tagsApi.getTags(
                context.projectId,
                Constants.TagOrderBy.UPDATED,
                Constants.SortOrder.DESC,
                null
            )
        } returns listOf(
            createTag("2020.2"),
            createTag("2020.1"),
        )

        assertEquals("2020.2.1", algorithmYearBased.getNextReleaseVersion(context, gitLabApi))
    }

    @Test
    fun testIncrementedHotfix() {
        every {
            gitLabApi.tagsApi.getTags(
                context.projectId,
                Constants.TagOrderBy.UPDATED,
                Constants.SortOrder.DESC,
                null
            )
        } returns listOf(
            createTag("2020.2.2"),
            createTag("2020.2.1"),
            createTag("2020.1"),
        )

        assertEquals("2020.2.3", algorithmYearBased.getNextReleaseVersion(context, gitLabApi))
    }

    @Test
    fun testMultipleIncrementedHotfix() {
        every {
            gitLabApi.tagsApi.getTags(
                context.projectId,
                Constants.TagOrderBy.UPDATED,
                Constants.SortOrder.DESC,
                null
            )
        } returns listOf(
            createTag("2020.3.1"),
            createTag("2020.3"),
            createTag("2020.2.2"),
            createTag("2020.2.1"),
            createTag("2020.1"),
        )

        assertEquals("2020.3.2", algorithmYearBased.getNextReleaseVersion(context, gitLabApi))
    }

    @Test
    fun testWithReleaseCandidate() {
        every {
            gitLabApi.tagsApi.getTags(
                context.projectId,
                Constants.TagOrderBy.UPDATED,
                Constants.SortOrder.DESC,
                null
            )
        } returns listOf(
            createTag("2020.3.RC2"),
            createTag("2020.3.RC1"),
            createTag("2020.2.2"),
            createTag("2020.2.1"),
            createTag("2020.1"),
        )

        assertEquals("2020.2.3", algorithmYearBased.getNextReleaseVersion(context, gitLabApi))
    }


    private fun createTag(name: String): Tag {
        val tag = Tag()
        tag.name = name
        return tag
    }
}