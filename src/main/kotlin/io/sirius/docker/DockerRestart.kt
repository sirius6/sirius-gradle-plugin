package io.sirius.docker

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.AbstractExecTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

open class DockerRestart : DefaultTask() {

    @Input
    lateinit var service: String

    @TaskAction
    fun execute() {
        project.exec {
            commandLine(
                "docker-compose",
                "restart",
                service
            )
        }
    }
}