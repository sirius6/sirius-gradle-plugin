package io.sirius.docker

import org.gradle.api.Action
import org.gradle.api.DefaultTask
import java.io.*

open class DockerExec : DefaultTask() {

    /** Execute a command in a Docker container */
    fun dockerExec(container: String, action: Action<DockerExecSpec>) {
        val spec = DockerExecSpec()
        action.execute(spec)

        project.exec {
            val args = mutableListOf("docker", "exec")

            // Add environment variables
            spec.env.forEach { (key, value) -> args.addAll(listOf("-e", "$key=$value")) }

            args.add(container)
            args.addAll(spec.commands)

            spec.input?.let { standardInput = it.getInputStream() }
            spec.output?.let { standardOutput = it.getOutputStream() }

            // Execute command
            commandLine(args)
            standardOutput.flush()
        }
    }

    class DockerExecSpec {
        val env = mutableMapOf<String, String>()
        var commands = emptyList<String>()
        var input: Input? = null
        var output: Output? = null

        fun inputString(str: String): Input = object: Input {
            override fun getInputStream(): InputStream = ByteArrayInputStream(str.toByteArray())
        }

        fun inputFile(path: String): Input = object: Input {
            override fun getInputStream(): InputStream = FileInputStream(path)
        }

        fun outputFile(file: File): Output = object: Output {
            override fun getOutputStream(): OutputStream = FileOutputStream(file)
        }
    }

    interface Input {
        fun getInputStream(): InputStream
    }

    interface Output {
        fun getOutputStream(): OutputStream
    }

}
