package io.sirius

import io.sirius.dsl.appVersion
import io.sirius.dsl.gitlabPrivateToken
import io.sirius.dsl.gitlabProjectId
import io.sirius.gitlab.GitlabReleaseTask
import io.sirius.gitlab.release.ReleaseVersionAlgorithm
import io.sirius.gitlab.release.YearBasedHotfixReleaseAlgorithm
import io.sirius.gitlab.release.YearBasedReleaseAlgorithm
import org.gradle.api.*
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.credentials.HttpHeaderCredentials
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.authentication.http.HttpHeaderAuthentication
import org.gradle.kotlin.dsl.*
import java.io.File
import java.net.URI

class SiriusPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        // Add tasks
        project.configureGitlabTasks()
        project.configureDockerComposeTasks()

        // Configure Gradle project
        project.allprojects {
            version = project.appVersion
            group = "io.sirius"

            configureIntegrationTests()

            afterEvaluate {
                configureRepositories()
                configurePublishing()
            }
        }
    }

    private fun Project.configureGitlabTasks() {
        val gitlabGroup = "gitlab"
        val extension = extensions.create<GitlabExtension>("gitlab")

        tasks.register<GitlabReleaseTask>("gitlabRelease") {
            group = gitlabGroup
            description = "Create a release for major version"
            algorithm = extension.release
            dryRun = extension.dryRun
        }
        tasks.register<GitlabReleaseTask>("gitlabHotfixRelease") {
            group = gitlabGroup
            description = "Create a release for hotfix version"
            algorithm = extension.hotfixRelease
            dryRun = extension.dryRun
        }
    }

    private fun Project.configureDockerComposeTasks() {
        // This extension is always present. If not, it causes build problems
        val extension = extensions.create<DockerComposeExtension>("dockerCompose")
        val dockerGroup = "docker"

        val dockerComposeFile = File(projectDir, "docker-compose.yml")
        if (dockerComposeFile.exists()) {

            tasks.register<DefaultTask>("dockerComposeUp") {
                group = dockerGroup

                val args = mutableListOf("docker-compose", "up", "-d")
                args.addAll(extension.services)

                doLast {
                    exec {
                        environment(extension.environment)
                        commandLine(args)
                    }
                }
            }

            tasks.register<DefaultTask>("dockerComposeDown") {
                group = dockerGroup

                doLast {
                    exec {
                        environment(extension.environment)
                        commandLine("docker-compose", "down")
                    }
                }
            }

            tasks.register<DefaultTask>("dockerComposePull") {
                group = dockerGroup

                doLast {
                    exec {
                        environment(extension.environment)
                        commandLine("docker-compose", "pull")
                    }
                }
            }
        }
    }

    private fun Project.configureRepositories() {
        repositories {
            mavenCentral()
            maven {
                url = URI.create("https://gitlab.com/api/v4/groups/sirius6/-/packages/maven")
                name = "Gitlab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Private-Token"
                    value = gitlabPrivateToken
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }

    private fun Project.configurePublishing(projectId: String = gitlabProjectId) {
        if (extensions.findByType<PublishingExtension>() != null) {
            extensions.configure(PublishingExtension::class.java) {

                publications {
                    val bootJarTask: Task? = tasks.findByName("bootJar")
                    if (bootJarTask != null) {
                        create<MavenPublication>("bootJava") {
                            artifact(bootJarTask)
                        }
                    } else {
                        create<MavenPublication>("maven") {
                            from(components["java"])
                        }
                    }
                }

                repositories {
                    maven {
                        url = URI.create("https://gitlab.com/api/v4/projects/${projectId}/packages/maven")
                        name = "Gitlab"
                        credentials(HttpHeaderCredentials::class) {
                            name = "Private-Token"
                            value = gitlabPrivateToken
                        }
                        authentication {
                            create<HttpHeaderAuthentication>("header")
                        }
                    }
                }
            }
        }
    }

    private fun Project.configureIntegrationTests() {
        val sourceSets: SourceSetContainer = project.extensions.findByType(SourceSetContainer::class.java) ?: return

        val integSourceSet: SourceSet = sourceSets.create("testIntegration") {
            compileClasspath += sourceSets.main.get().output
            runtimeClasspath += sourceSets.main.get().output
        }

        val testIntegrationImplementation by configurations.getting {
            extendsFrom(configurations.testImplementation.get())
        }

        val integrationTest = tasks.register<Test>("integrationTest") {
            description = "Runs integration tests"
            group = "verification"

            testClassesDirs = integSourceSet.output.classesDirs
            classpath = integSourceSet.runtimeClasspath
            shouldRunAfter("test")
        }

        tasks.findByName("check")?.dependsOn(integrationTest)
    }

    private val SourceSetContainer.main: NamedDomainObjectProvider<SourceSet>
        get() = named("main")

    private val ConfigurationContainer.testImplementation: NamedDomainObjectProvider<Configuration>
        get() = named("testImplementation")
}

open class DockerComposeExtension {
    /** Environment variable to use in the compose */
    var environment: Map<String, String> = emptyMap()

    /** The services names to start in compose */
    var services: List<String> = emptyList()
}

open class GitlabExtension {
    /** Set to "true" to simulate the release */
    var dryRun: Boolean = false

    /** Algorithm for major release version generator */
    var release: ReleaseVersionAlgorithm = YearBasedReleaseAlgorithm()

    /** Algorithm for hotfix release version generator */
    var hotfixRelease: ReleaseVersionAlgorithm = YearBasedHotfixReleaseAlgorithm()
}