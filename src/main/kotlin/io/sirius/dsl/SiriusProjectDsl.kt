package io.sirius.dsl

import io.sirius.DockerComposeExtension
import io.sirius.GitlabExtension
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.named
import org.gradle.testing.jacoco.tasks.JacocoReport
import java.io.File


val Project.gitlabPrivateToken: String
    get() = System.getenv("CI_BUILD_TOKEN")
        ?: project.findProperty("gitLabPrivateToken")?.toString()
        ?: "to_define_in_home"

val Project.gitlabProjectId: String
    get() = System.getenv("CI_PROJECT_ID")
        ?: "to_define_in_home"

val Project.appVersion: String
    get() {
        val releaseFile = File(projectDir, "RELEASE")
        return if (releaseFile.exists()) releaseFile.readText()
        else "development"
    }

fun Project.dockerCompose(action: DockerComposeExtension.() -> Unit) {
    configure(action)
}

fun Project.gitlab(action: GitlabExtension.() -> Unit) {
    configure(action)
}

fun Project.configureJacoco() {
    tasks.named<JacocoReport>("jacocoTestReport") {
        reports {
            xml.isEnabled = true
            html.isEnabled = false
        }
        executionData.setFrom(fileTree(buildDir).include("/jacoco/*.exec"))
    }
}
