package io.sirius.gitlab


class JobContext(
    val gitlabUrl: String = System.getenv("CI_SERVER_URL"),
    val projectId: String = System.getenv("CI_PROJECT_ID"),
    val projectName: String = System.getenv("CI_PROJECT_NAME"),
    val buildBranch: String = System.getenv("CI_COMMIT_BRANCH"),

    // Value set in Gitlab CI variables
    // https://gitlab.com/fabien.barbero/authentication-service/-/settings/ci_cd
    val accessToken: String = System.getenv("CI_BUILD_TOKEN")
)