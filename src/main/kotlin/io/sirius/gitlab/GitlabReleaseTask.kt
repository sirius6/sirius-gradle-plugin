package io.sirius.gitlab

import io.sirius.gitlab.release.ReleaseVersionAlgorithm
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.ReleaseParams
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 * This task changes the project version depending on the [ReleaseVersionAlgorithm].
 * It also store the version in the RELEASE file at the project root directory.
 */
open class GitlabReleaseTask : DefaultTask() {

    @get:Input
    lateinit var algorithm: ReleaseVersionAlgorithm

    @get:Input
    var dryRun: Boolean = false

    @TaskAction
    fun execute() {
        val context = JobContext()
        val gitlabApi = GitLabApi(context.gitlabUrl, context.accessToken)

        val releaseVersion = algorithm.getNextReleaseVersion(context, gitlabApi)
        if (dryRun) {
            logger.info("[DRY RUN] Project can be released with version $releaseVersion")

        } else {
            val releaseFile = File(project.projectDir, "RELEASE")
            releaseFile.writeText(releaseVersion)
            project.version = releaseVersion
            releaseProject(context, gitlabApi)
            logger.info("Release created with value $releaseVersion")
        }
    }

    private fun releaseProject(
        context: JobContext,
        gitlabApi: GitLabApi
    ) {
        val releaseVersion = project.version.toString()

        // Create tag
        val tag = gitlabApi.tagsApi.createTag(
            context.projectId,
            releaseVersion,
            context.buildBranch,
        )

        // Get changelog file to use it in release description
        val releaseNotesFile = File(project.projectDir, "CHANGELOG.md")
        val releaseNotes = if (releaseNotesFile.exists()) releaseNotesFile.readText() else null

        // Create release
        val releaseParams = ReleaseParams()
            .withName(releaseVersion)
            .withTagName(tag.name)
            .withDescription(releaseNotes)
        gitlabApi.releasesApi.createRelease(context.projectId, releaseParams)

        logger.info("New tag {} created", tag.name)
    }

}