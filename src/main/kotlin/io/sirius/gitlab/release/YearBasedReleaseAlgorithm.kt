package io.sirius.gitlab.release

import io.sirius.gitlab.JobContext
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import java.time.Year

/** It generates major release version based on the year and an increment (ie. "2021.1", "2021.2") */
class YearBasedReleaseAlgorithm : ReleaseVersionAlgorithm {

    override fun getNextReleaseVersion(
        context: JobContext,
        gitlabApi: GitLabApi
    ): String {
        val year = Year.now()
        val versionRegex = Regex("""^\d{4}\.(\d+)$""")

        // Get last release ... or 0 if no release found
        val lastVersion = gitlabApi.tagsApi.getTags(
            context.projectId,
            Constants.TagOrderBy.UPDATED,
            Constants.SortOrder.DESC,
            "^$year." // Only fetch the versions on the current year
        ).asSequence()
            .mapNotNull { versionRegex.find(it.name)?.groupValues?.getOrNull(1)?.toInt() }
            .firstOrNull() ?: 0

        val incrementalVersion = lastVersion + 1
        return "$year.$incrementalVersion"
    }
}