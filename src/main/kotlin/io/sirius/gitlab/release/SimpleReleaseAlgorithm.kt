package io.sirius.gitlab.release

import io.sirius.gitlab.JobContext
import org.gitlab4j.api.GitLabApi

/**
 * Simple algorithm using the version set in constructor parameter.
 * Can be used directly in [io.sirius.GitlabExtension].
 */
class SimpleReleaseAlgorithm(private val version: String) : ReleaseVersionAlgorithm {

    override fun getNextReleaseVersion(
        context: JobContext,
        gitlabApi: GitLabApi
    ): String = version
}