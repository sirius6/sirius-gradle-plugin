package io.sirius.gitlab.release

import io.sirius.gitlab.JobContext
import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi

/**
 * Create version by incrementing the hotfix value. For example, if the last release is "2020.1", the hotfix will be "2020.1.1".
 * And if the last release is "2020.2.1", the hotfix will be "2020.2.2".
 *
 * Of course, a release must be already present! If not, the task will fail.
 */
class YearBasedHotfixReleaseAlgorithm : ReleaseVersionAlgorithm {

    override fun getNextReleaseVersion(
        context: JobContext,
        gitlabApi: GitLabApi
    ): String {
        val generalVersionRegex = Regex("""^\d{4}(\.(\d+)){1,2}$""")
        val hotfixVersionRegex = Regex("""^\d{4}\.\d+\.(\d+)$""")

        // First we search the last release (with or without hotfix value)
        val lastVersion: String = gitlabApi.tagsApi.getTags(
            context.projectId,
            Constants.TagOrderBy.UPDATED,
            Constants.SortOrder.DESC,
            null
        ).asSequence()
            .map { it.name }
            .first { it.matches(generalVersionRegex) }

        val hotfixResult = hotfixVersionRegex.find(lastVersion)

        return if (hotfixResult != null) {
            // Increment hotfix version
            val oldHotfixVersion: Int = hotfixResult.groupValues[1].toInt()

            val range: IntRange = hotfixResult.groups[1]?.range
                ?: throw IllegalArgumentException("Error getting hotfix range")
            lastVersion.replaceRange(range, (oldHotfixVersion + 1).toString())

        } else {
            // Very first hotfix for the version
            "$lastVersion.1"
        }
    }
}