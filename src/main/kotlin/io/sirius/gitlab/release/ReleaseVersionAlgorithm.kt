package io.sirius.gitlab.release

import io.sirius.gitlab.JobContext
import org.gitlab4j.api.GitLabApi

interface ReleaseVersionAlgorithm {

    /** Calculate the next project version for the release */
    fun getNextReleaseVersion(
        context: JobContext,
        gitlabApi: GitLabApi
    ): String

}