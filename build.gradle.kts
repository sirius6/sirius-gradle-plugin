import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.entity.ContentType
import org.apache.http.impl.client.HttpClientBuilder

plugins {
    id("maven-publish")
    id("io.github.http-builder-ng.http-plugin") version "0.1.1"
    `java-gradle-plugin`
    `kotlin-dsl`
}

group = "io.sirius"
version = "2.2.1"

java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

gradlePlugin {
    plugins {
        register("siriusPlugin") {
            id = "sirius"
            implementationClass = "io.sirius.SiriusPlugin"
        }
    }
}

repositories {
    mavenCentral()
}

tasks.register<DefaultTask>("performRelease") {
    group = "publishing"
    description = "Publish the plugin in the package repository and create GIT tag"

    dependsOn("assemble")
    dependsOn("publish")

    doLast {
        val post = HttpPost("https://gitlab.com/api/v4/projects/23069682/repository/tags")
        post.entity = StringEntity("""{"ref":"master","tag_name":"$version"}""", ContentType.APPLICATION_JSON)
        post.addHeader("PRIVATE-TOKEN", gitLabPrivateToken)

        val client = HttpClientBuilder.create().build()
        client.execute(post).use { require(it.statusLine.statusCode == 200) }
    }
}

dependencies {
    implementation(gradleApi())
    implementation("org.gitlab4j:gitlab4j-api:4.16.0")

    testImplementation("io.mockk:mockk:1.10.0")
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
}

val gitLabPrivateToken: String by project

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/23069682/packages/maven")
            name = "Gitlab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitLabPrivateToken
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}